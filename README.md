# Secure Cluster Kubernetes



## Getting started
gcloud container clusters get-credentials cluster-1 --zone europe-central2-a --project asgroup20


gcloud auth configure-docker europe-west1-docker.pkg.dev
docker build -t dvwa-https-group20 .
docker tag dvwa-https-group20:latest europe-southwest1-docker.pkg.dev/asgroup20/asgroup20-docker/dvwa-https-group20
docker push europe-southwest1-docker.pkg.dev/asgroup20/asgroup20-docker/dvwa-https-group20

https://github.com/kubescape/kubescape
curl -s https://raw.githubusercontent.com/kubescape/kubescape/master/install.sh | /bin/bash

https://cloud.google.com/intrusion-detection-system/docs/configuring-ids#console



NFS SERVER

gcloud compute disks create --size=10GB --zone=us-east1-b gce-nfs-disk


https://kubernetes.io/docs/tutorials/security/seccomp/