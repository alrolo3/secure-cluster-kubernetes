gcloud compute networks create cluster-kube --project=asgroup-20 --subnet-mode=custom  \
--mtu=1500 --bgp-routing-mode=global

gcloud compute networks subnets create cluster --project=asgroup-20 --range=10.0.1.0/24 --stack-type=IPV4_ONLY \
--network=cluster-kube --region=europe-central2 --enable-private-ip-google-access --enable-flow-logs \
--logging-aggregation-interval=interval-30-sec --logging-flow-sampling=0.5 --logging-metadata=include-all

gcloud compute networks subnets create siem --project=asgroup-20 --range=10.0.2.0/24 --stack-type=IPV4_ONLY \
--network=cluster-kube --region=europe-southwest1 --enable-private-ip-google-access --enable-flow-logs \
--logging-aggregation-interval=interval-30-sec --logging-flow-sampling=0.5 --logging-metadata=include-all

gcloud beta container --project "asgroup-20" clusters create "cluster-1" --no-enable-basic-auth --cluster-version "1.29.1-gke.1589020" --release-channel "regular" --machine-type "e2-medium" 
--image-type "COS_CONTAINERD" --disk-type "pd-balanced" --disk-size "50" --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only",
"https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly",
"https://www.googleapis.com/auth/trace.append" --num-nodes "3" 
--logging=SYSTEM,WORKLOAD --monitoring=SYSTEM --enable-ip-alias --network "projects/asgroup-20/global/networks/cluster-kube" --subnetwork "projects/asgroup-20/regions/europe-central2/subnetworks/cluster" 
--enable-intra-node-visibility --default-max-pods-per-node "110" --security-posture=standard --workload-vulnerability-scanning=standard --enable-dataplane-v2 --no-enable-master-authorized-networks 
--addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver,BackupRestore --enable-autoupgrade 
--enable-autorepair --max-surge-upgrade 1 --max-unavailable-upgrade 0 --binauthz-evaluation-mode=DISABLED --no-enable-managed-prometheus --enable-shielded-nodes --notification-config=pubsub=ENABLED,
pubsub-topic=projects/asgroup-20/topics/cluster-kube-logs --node-locations "europe-central2-a"