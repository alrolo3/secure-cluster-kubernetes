#!/bin/bash

mkdir certs \
&& openssl genrsa -out certs/ca.key 2048 \
&& openssl req -x509 -new -nodes -key certs/ca.key -sha256 -days 1024 -out certs/ca.crt -subj "/C=ES/L=Madrid/O=UC3M/OU=CA/CN=CA DVWA" \
&& openssl genrsa -out certs/apache.key 2048 \
&& openssl req -new -key certs/apache.key -out certs/apache.csr -subj "/C=ES/L=Madrid/O=UC3M/OU=AS/CN=localhost" \
&& openssl x509 -req -in certs/apache.csr -CA certs/ca.crt -CAkey certs/ca.key -CAcreateserial -out certs/apache.crt -days 365 -sha256
