#!/bin/bash

read -p "Enter image name: " image_name
if [ -z "$image_name" ]; then
  echo "Error: Image name is missing."
  exit 1
fi

read -p "Enter artifact repo: " artifact_domain_zone
if [ -z "$artifact_domain_zone" ]; then
  echo "Error: Artifact domain zone is missing."
  exit 1
fi

git clone https://github.com/digininja/DVWA.git

cp -f build/compose.yml DVWA/compose.yml
cp -f build/default-ssl.conf DVWA/default-ssl.conf
cp -f build/ports.conf DVWA/ports.conf
cp -f build/Dockerfile DVWA/Dockerfile
cp -f build/loaderio-cc368f5a083c26bfc51cd5914e9c57bc.txt DVWA/loaderio-cc368f5a083c26bfc51cd5914e9c57bc.txt

sh build/generate-certs.sh
mkdir DVWA/certs

cp -f certs/ca.crt DVWA/certs/ca.crt
cp -f certs/apache.crt DVWA/certs/apache.crt
cp -f certs/apache.key DVWA/certs/apache.key

docker build -t $image_name DVWA/
docker tag ${image_name}:latest ${artifact_domain_zone}/${image_name}
docker push ${artifact_domain_zone}/${image_name}

